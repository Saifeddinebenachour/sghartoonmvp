﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using System;

public class video_Intro_Ctr : MonoBehaviour
{
    public VideoPlayer player;
    public VideoClip mozmoz_Video;
    public VideoClip farawla_Video;
    public GameObject perso_choice_panel;
    public bool video_Is_Playng;

    public GameObject Game;
    // Start is called before the first frame update
    void Start()
    {
        video_Is_Playng = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (video_Is_Playng)
        {

            if (player.GetComponent<VideoPlayer>().frame < Convert.ToInt64(player.GetComponent<VideoPlayer>().frameCount))
            {
                print("VIDEO IS PLAYING");
            }
            else if (player.targetCameraAlpha > 0)
            {
                //  player.gameObject.SetActive(false);
                player.targetCameraAlpha -= 0.01f;
            }
            else
            {
                Game.SetActive(true);
                perso_choice_panel.SetActive(false);
            }

        }
    }


    public void playMozMozVideo()
    {

        video_Is_Playng = true;
        player.clip = mozmoz_Video;
        perso_choice_panel.SetActive(false);
    }
    public void playFarawlaVideo()
    {

        video_Is_Playng = true;
        player.clip = farawla_Video;
        perso_choice_panel.SetActive(false);

    }
}
